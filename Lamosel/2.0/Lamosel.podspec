#
#  Be sure to run `pod spec lint LAMOSEL.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
 
  s.name         = "Lamosel"
  s.version      = "2.0"
  s.summary      = "LAMOSEL : Lagardere Active Mobile"
 
  s.description  = <<-DESC
                   LAMOSEL : Lagardere Active Mobile Services Exchange Library iOS Library for automate communication between iOS App and Lagardere server for VMV services (Voting-Messagin-Rating) and mediametrie URL
                   DESC


  s.homepage     = "https://gitlab.com/ladpods/lamosel"
 
  s.license      = { :type => 'Lagardère Active', :text => <<-LICENSE
    license Lagardère
    LICENSE
  }

  s.author             = { "obellerose" => "extia-odile.bellerose@lagardere-active.com" }    
 
  s.source       = { :git => "https://gitlab.com/ladpods/lamosel.git", :tag => s.version }

  s.source_files  = "Classes", "Classes/**/*.{h,m}"

  s.public_header_files = "Classes/**/*.h"

  s.requires_arc = false

  s.platform     = :ios, "7.0"

  s.resource = "Classes/LAMOSEL.bundle"

end
