#
# Be sure to run `pod lib lint LAVideoPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "LAVideoPlayer"
  s.version          = "1.0"
  s.summary          = "MRAID Video player."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
video player avec Google analytics et le SDK IMA

                       DESC
  s.homepage         = "https://gitlab.com/ladpods/lavideoplayer"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Stéphane Couzinier" => "stephane.couzinier@lagardere-active.com" }
  s.source           = { :git => "git@gitlab.in.ladtech.fr:Mobile/LAVideoPlayer.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.dependency 'google-cast-sdk', '~> 2.10'
  s.dependency 'GoogleAnalytics' , '~> 3.13'
  s.dependency 'GoogleAds-IMA-iOS-SDK-For-AdMob', '3.3.1'
  s.dependency 'GoogleTagManager', '~> 3.09'

  s.resources = 'Pod/Assets/**/*.png'
  s.source_files = 'Pod/Classes/**/*.{h,m}'
  s.resource_bundles = {
  'LAVideoPlayer' => ['Pod/Assets/**/*.*','Pod/Classes/**/*.{xib}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
