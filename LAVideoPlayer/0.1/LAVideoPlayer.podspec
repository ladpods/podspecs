Pod::Spec.new do |s|  
  s.name         = "LAVideoPlayer"
  s.version      = "0.1"
  s.summary      = "LA Player"

  s.description  = <<-DESC
                   Lagardère active video player custom
                   DESC

  s.source       = { :git => "https://gitlab.com/ladpods/lavideoplayer.git", :tag => s.version }
  s.homepage     = "https://gitlab.com/ladpods/lavideoplayer"
  s.license      = { :type => 'Lagardère Active', :text => <<-LICENSE
    license Lagardère
    LICENSE
  }
  s.author       = { "odile bellerose" => "extia-odile.bellerose@lagardere-active.com" }
  s.platform     = :ios, '7.0'
  s.source_files  = 'LAVideoPlayer', 'LAVideoPlayer/**/*.{h,m}'
  s.public_header_files = 'LAVideoPlayer/**/*.h'
  
  s.requires_arc = true

  s.dependency 'google-cast-sdk', '~> 2.10'
  s.dependency 'GoogleAnalytics' , '~> 3.13'
  s.dependency 'GoogleAds-IMA-iOS-SDK', '3.0.beta.16'

  s.resource_bundles = {
      'LAVideoPlayer' => [
        'LAVideoPlayer/Assets/Data/*',
        'LAVideoPlayer/Assets/Images/Images.xcassets',
      ]
    } 

end  
