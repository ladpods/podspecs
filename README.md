Pour utiliser les pods privés :

Ajouter le répo  à votre cocoapods local :

- pod repo add podspec https://gitlab.com/ladpods/podspecs.git

Copier/coller les lignes suivantes en haut du podfile  :

- source 'https://github.com/CocoaPods/Specs.git' => Important pour les pods utilisant des dépendances comme Google-Cast, Google Analytics... (Cf. LAVideoPlayer)
- source 'https://gitlab.com/ladpods/podspecs'

Ajouter les pods que vous souhaitez utiliser dans votre projet :

Exemple : 

- pod 'Lamosel'
- pod 'LAVideoPlayer'

-------------------------------------

Créer un pod privé

- pod repo add podspec https://gitlab.com/ladpods/podspecs (Si le repo n'existe pas déjà dans cocoapods)

- Exécuter la commande de création d’un pod  et suivre les  différentes instructions :
 ```ruby
pod lib create ZYVideoPlayer
```

Le pod créé contient plusieurs éléments notamment : 
- un dossier Assets qui contiendra les ressources images, fichiers de localisation etc. 
- un dossier Classes qui contiendra les classes du projet
- un ficher podspec qui contiendra les informations relatives au pod  (version, descriptions, dépendances etc.)

- Dans le podspec indiquer la version du pod 

Exemple :
```ruby
s.version          = "0.1.14"
```

- Mettre à jour le répo du projet 
```ruby
git push origin master
```

- Tagger avec le numero de version du pod
```ruby
git tag 0.1.14
```

- Pusher le tag
```ruby
git push  --tags
```


- Pusher le pod dans le repo:
```ruby
pod repo push [NOM DU POD] [NOM DU POD SPEC].podspec --verbose --use-libraries
ex : pod repo push podspec LAVideoPlayer.podspec --verbose --use-libraries --allow-warnings
```

NB : 
--verbose permet d'avoir le détail de l'éxécution et de voir les éventuelles erreur

--use-libraries permet d'indiquer que le podspec utilise des librairies statiques (evite les erreurs de validation du podspec)

--Pour un projet Swift vérifier la version de Swift utilisée avec cette commande xcrun swift -version puis créer un fichier .swift-version (echo "your.swift.version" > .swift-version) pour indiquer la version sinon le push ne fonctionnera pas si la version utilisée ne correspond pas à la version que pod utilise par défaut 

NB : Avec les dernières mise à jour de cocoapods il se peut que pour un pod qui fonctionnait avant cette commande ne fonctionne pas 
(notamment lorsqu’il y a des librairies embarquées cf. AdNetworkSupport). 
L’alternative est de créer manuellement le dossier avec le nouveau numéro de version dans le répo et d’y ajouter la dernière version du podspec. 