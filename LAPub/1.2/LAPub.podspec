#
# Be sure to run `pod lib lint LAPub.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "LAPub"
  s.version          = "1.2"
  s.summary          = "Code pour la pub"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
regroupement de code pour la pub
DESC

  s.homepage         = "https://gitlab.com/ladpods/lapub"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Stéphane Couzinier" => "stephane.couzinier@lagardere-active.com" }
  s.source           = { :git => "https://gitlab.com/ladpods/lapub.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  #s.resource_bundles = {
  #  'LAPub' => ['Pod/Assets/*.png']
  #}

s.dependency 'Google-Mobile-Ads-SDK', '~> 7.0'
s.dependency 'mopub-ios-sdk', '~> 4.0'
s.dependency 'AFNetworking', '~> 3.0'

  s.public_header_files = 'Pod/Classes/**/*.h'
s.frameworks =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
  # s.dependency 'AFNetworking', '~> 2.3'
end
