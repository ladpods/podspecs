#
# Be sure to run `pod lib lint LARegistration.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "LARegistration"
  s.version          = "0.1.11"
  s.summary          = "Lagardere Active Registration module"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                      LARegistration : standard login, login with Google and Facebook, logout, sign in... 
                       DESC

  s.homepage         = "https://gitlab.com/ladpods/laregistration"
  s.license          = 'MIT'
  s.author           = { "odile bellerose" => "extia-odile.bellerose@lagardere-active.com" }
  s.source           = {  :git => "https://gitlab.com/ladpods/laregistration.git", :tag => s.version.to_s  }
  
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*.{h,m}'
  s.resource_bundles = {
    'LARegistration' => ['Pod/Assets/**/*.plist']
  }

  s.dependency 'AFNetworking',  '~> 2.0'
  s.dependency 'KVNProgress', '~> 2.2.1'
  s.dependency 'GoogleTagManager', '~> 3.09'
  s.dependency 'FBSDKCoreKit' , '~> 4.8.0'
  s.dependency 'FBSDKLoginKit', '~> 4.8.0'
  s.dependency 'FBSDKShareKit', '~> 4.8.0'
  s.dependency 'Google/SignIn', '~> 1.0'
  s.dependency 'GoogleTagManager', '~> 3.09'

end
