Pod::Spec.new do |s|
  s.name             = 'LADAnalyticsDebug'
  s.version          = '1.0.1'
  s.summary          = 'A short description of LADAnalyticsDebug.'

  s.homepage         = 'https://gitlab.com/ladpods/ladanalyticsdebug'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'laddev' => 'jean-baptiste.castro@lagardere-active.com' }
  s.source           = { :git => 'https://gitlab.com/ladpods/ladanalyticsdebug.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'LADAnalyticsDebug/Classes/**/*'
  
  s.resource_bundles = {
    'LADAnalyticsDebug' => ['LADAnalyticsDebug/Assets/*.xib']
  }
end
