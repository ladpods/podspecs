Pod::Spec.new do |s|
    s.name             = 'LADAnalyticsDebug'
    s.version          = '1.0.3'
    s.summary          = 'A short description of LADAnalyticsDebug.'

    s.homepage         = 'https://gitlab.com/ladpods/ladanalyticsdebug'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'laddev' => 'jean-baptiste.castro@lagardere-active.com' }
    s.source           = { :git => 'https://gitlab.com/ladpods/ladanalyticsdebug.git', :tag => s.version.to_s }

    s.ios.deployment_target = '8.0'

    s.preserve_paths = 'LADAnalyticsDebug/Framework/LADAnalyticsDebug.framework'
    s.public_header_files = 'LADAnalyticsDebug/Framework/LADAnalyticsDebug.framework/Versions/A/Headers/LADAnalyticsDebug.h'
    s.resources = 'LADAnalyticsDebug/Framework/LADAnalyticsDebug.framework/Versions/A/Resources/LADAnalyticsDebug.bundle'
    s.vendored_frameworks = 'LADAnalyticsDebug/Framework/LADAnalyticsDebug.framework'
end
