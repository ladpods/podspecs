Pod::Spec.new do |s|

#   AdNetworkSupport description

    s.name      = "AdNetworkSupport"
    s.version   = "0.2.6"
    s.summary   = "AdNetworkSupport adapter"

    s.homepage  = "https://gitlab.com/ladpods/adnetworksupport"
    s.license   = 'MIT'
    s.author    = { "laddev" => "castro jean-baptiste <jean-baptiste.castro@lagardere-active.com>" }
    s.source    = { :git => "https://gitlab.com/ladpods/adnetworksupport.git", :tag => s.version.to_s }
    s.platform  = :ios, '8.0'

#   MoPub Adapter

    s.subspec 'MoPub' do |mp|
        mp.requires_arc         = true
        mp.vendored_libraries   = 'Pod/MoPub/Libraries/libAdapterSDKMoPub.a'
        mp.library              = 'AdapterSDKMoPub'
        mp.xcconfig             = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/AdNetworkSupport/Pod/MoPub/Libraries' }
    end

#   Millennial Adapter

    s.subspec 'Millennial' do |ml|
        ml.requires_arc         = true
        ml.source_files         = 'Pod/Millennial/Classes/*'
        ml.public_header_files  = 'Pod/Millennial/Classes/*.h'
        ml.vendored_frameworks  = 'Pod/Millennial/Libraries/MMAdSDK.framework'
        ml.dependency             'mopub-ios-sdk', '~> 5.0'
        ml.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration", "CoreBluetooth"
    end

#   InMobi Adapter

    s.subspec 'InMobi' do |im|
        im.requires_arc         = true
        im.source_files         = 'Pod/InMobi/Classes/*'
        im.public_header_files  = 'Pod/InMobi/Classes/*.h'
        im.vendored_libraries   = 'Pod/InMobi/Libraries/libInMobi.a'
        im.library              = 'InMobi'
        im.xcconfig             = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/AdNetworkSupport/Pod/InMobi/Libraries' }
        im.dependency             'mopub-ios-sdk', '~> 5.0'
        im.dependency             'sqlite3'
        im.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
    end

#   Facebook Adapter

    s.subspec 'Facebook' do |fb|
        fb.requires_arc         = true
        fb.source_files         = 'Pod/Facebook/Classes/*'
        fb.public_header_files  = 'Pod/Facebook/Classes/*.h'
        fb.dependency             'mopub-ios-sdk', '~> 5.0'
        fb.dependency             'FBAudienceNetwork'
        fb.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
    end

#   Google Adapter

    s.subspec 'Google' do |go|
        go.requires_arc         = true
        go.source_files         = 'Pod/Google/Classes/*'
        go.public_header_files  = 'Pod/Google/Classes/*.h'
        #go.vendored_frameworks  = 'Pod/Google/Libraries/GoogleMobileAds.framework'
        go.dependency             'Google-Mobile-Ads-SDK', '~> 7.27.0'
        go.dependency             'mopub-ios-sdk', '~> 5.0'
        go.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
    end

#   iAd Adapter

    s.subspec 'iAd' do |ia|
        ia.requires_arc         = true
        ia.source_files         = 'Pod/iAd/Classes/*'
        ia.public_header_files  = 'Pod/iAd/Classes/*.h'
        ia.dependency             'mopub-ios-sdk', '~> 5.0'
        ia.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
    end

#   Flurry Adapter

    s.subspec 'Flurry' do |fl|
        fl.requires_arc         = true
        fl.source_files         = 'Pod/Flurry/Classes/*'
        fl.public_header_files  = 'Pod/Flurry/Classes/*.h'
        fl.vendored_libraries   = 'Pod/Flurry/Libraries/libFlurryAds_7.3.0.a', 'Pod/Flurry/Libraries/libFlurry_7.3.0.a'
        fl.libraries            = 'FlurryAds_7.3.0', 'Flurry_7.3.0'
        fl.xcconfig             = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/AdNetworkSupport/Pod/Flurry/Libraries', 'OTHER_LDFLAGS' => '-lz' }
        fl.dependency             'mopub-ios-sdk', '~> 5.0'
        fl.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
    end

end
