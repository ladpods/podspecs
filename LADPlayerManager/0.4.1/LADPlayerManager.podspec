Pod::Spec.new do |s|
  s.name             = 'LADPlayerManager'
  s.version          = '0.4.1'
  s.summary          = 'A short description of LADPlayerManager.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/Jean-baptiste Castro/LADPlayerManager'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jean-baptiste Castro' => 'jean-baptiste.castro@lagardere-active.com' }
  s.source           = { :git => 'https://gitlab.com/ladpods/ladplayermanager', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'LADPlayerManager/Classes/**/*'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'JWPlayer-SDK', '~> 2.0'
end
