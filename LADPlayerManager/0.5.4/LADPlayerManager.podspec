Pod::Spec.new do |s|
  s.name             = 'LADPlayerManager'
  s.version          = '0.5.4'
  s.summary          = 'Video Player based on JWPlayer SDK.'

    s.homepage         = 'https://github.com/Jean-baptiste Castro/LADPlayerManager'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Jean-Baptiste Castro' => 'jean-baptiste.castro@lagardere-active.com' }
    s.source           = { :git => 'https://gitlab.com/ladpods/ladplayermanager', :tag => s.version.to_s }

    s.ios.deployment_target = '8.0'

    s.source_files = 'LADPlayerManager/Classes/**/*'

    s.resource_bundles = {
        'LADPlayerManager' => ['LADPlayerManager/Assets/*.{xib,png, nib}']
    }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'JWPlayer-SDK', '~> 2.0'
    s.dependency 'GoogleAds-IMA-iOS-SDK'
    s.dependency 'google-cast-sdk', '<= 2.10.0.4070'
end
